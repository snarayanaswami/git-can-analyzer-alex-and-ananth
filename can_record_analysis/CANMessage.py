"""
==========================================
CAN Message representing module
========================================== 

**file** : CANMessage.py

**date** : 5-May-2015
"""

from _struct import unpack


class CANMessage:
    """
    Class representing a sent CAN-Message containing the necessary data-fields
    
    Attributes:
        CANtype: Message type (standard or extended)
        CANid: CAN id (13- or 29-bit identifier)
        length: data length (1-8 byte)
        data: data field (1-8 bytes)
        count: how often has this message been received
        cycle: time passed since last retrieval of message type
        timestamp: relative timestamp
        abstimestamp: absolute timestamp
    """
    def __init__(self, CANtype, CANid, length, data, count, cycle, timestamp, abstimestamp):
        self.CANtype = CANtype
        self.CANid = CANid
        self.length = length
        self.data = data
        self.count = count
        self.cycle = cycle
        self.timestamp = timestamp
        self.abstimestamp = abstimestamp

    def __repr__(self):
        return 'ID: {}; Data: {}; Timestamp: {} \n'.format(self.CANid, self.data, self.abstimestamp)
    
    def __cmp__(self, other):
        if hasattr(other, 'CANid'):
            return self.CANid.__cmp__(other.CANid)

    def getTimeStampMicros(self):
        return float(self.timestamp.micros + 1000 * self.timestamp.millis + 0xFFFFFFFF * 1000 * self.timestamp.millis_overflow/1000)
        
    def getCSVRow(self):        
        csvRow =  [self.CANtype,str(self.CANid),str(self.length)]
        
        dataList = ['' for _ in range(8)]
        for index, value in enumerate(self.data):
            dataList[index] = value
        
        for dataValue in dataList:
            csvRow.append(dataValue)
            
        csvRow.append(str(self.count))
        csvRow.append(str(self.cycle))
        csvRow.append(str(self.abstimestamp))
        return csvRow
        
    def DictionarizeMsg(self):
        return {"CANtype": self.CANtype,
                "CANid": self.CANid,
                "length": self.length,
                "data": self.data,
                "count": self.count,
                "cycle": self.cycle,
                "timestamp": self.getTimeStampMicros(),
                "abstimestamp": self.abstimestamp}
        
    def __setitem__(self, key, value):
        if key=='CANtype':
            self.CANtype = value
        elif key=='CANid':
            self.CANid = value
        elif key=='length':
            self.length = value
        elif key=='data':
            self.data = value
        elif key=='count':
            self.count = value
        elif key=='cycle':
            self.cycle = value
        elif key=='abstimestamp':
            self.abstimestamp = value
            
class decodedCANMessage:
    """
    Class representing a decoded message with extended CAN identifier and data encoded in the data fields
    
    Attributes:
        origin: ID of sender ECU (0x00 - 0xFF)
        target: ID of target ECU (0x00 - 0xFE) [0xFF is broadcast-ID]
        msgtype: message type (MSG_TYPE for info)
        values: encapsulated values (varying with message type)
    """ 
    def __init__(self, msg = None):
        """
        Takes an existing message and decodes according to the corresponding description in MSG_TYPE
        
        Args:
            msg: Existing CAN message that will be decoded
        """
        if msg is not None:
            # Message origin is 4th byte in ID
            self.origin  = (msg.CANid    ) & 0xFF;
            # Message target is 3rd byte in ID
            self.target  = (msg.CANid>>8 ) & 0xFF;
            # Message type is in first 2 bytes of extended CANid
            self.msgtype = (msg.CANid>>16) & 0xFFFF;
            
            self.values = []
            position = 0
            # Iterate over number of values encapsulated in the message
            for i in range(2,self.MSG_TYPE[self.msgtype][1]+2):
                length = self.SIZES[self.MSG_TYPE[self.msgtype][i]]
                # Try to unpack the hex-encoded values into the specified message type
                try:
                    self.values.append(unpack(self.MSG_TYPE[self.msgtype][i], ''.join(chr(int(msg.data[n+position],16)) for n in range(length) ))[0])
                except:
                    self.values = []
                position += length
        else:
            pass
            
    def getExtID(self, msgtype = 'NONE', origin = 0x00, target = 0x00):
        """
        Build an extended (29-bit) identifier with the given properties
        
        Args:
            idType: message type
            origin: ID of sender ECU (0x00 - 0xFF)
            target: ID of target ECU (0x00 - 0xFE) [0xFF is broadcast-ID]
        """
        for key in self.MSG_TYPE:
            if self.MSG_TYPE[key][0] == msgtype:
                return (key<<16) | (target<<8) | (origin)
        return 0x00000000
        
    def __repr__(self):
        return '{}\norigin: {}\ntarget: {}\nvalues: {}'.format(self.MSG_TYPE[self.msgtype][0],self.origin, self.target, self.values)
    

    
    # SmartCell communication protocol definitions
    MSG_TYPE = {0x0000 : ('NONE',0),
                
                0x0001 : ('MSG_TYPE_UNBLOCK',2,'B','B'),
                0x0002 : ('MSG_TYPE_BLOCK',2,'B','B'),
                0x0003 : ('MSG_TYPE_STATUS_RESPONSE',3,'B','B','B'),
                
                0x0010 : ('MSG_TYPE_SOC',1,'f'),
                0x0011 : ('MSG_TYPE_VOLTAGE',1,'f'),
                0x0012 : ('MSG_TYPE_TEMPERATURE',1,'f'),
                
                0x0020 : ('MSG_TYPE_SEND_REQ',0),
                0x0021 : ('MSG_TYPE_SEND_ACK',0),
                0x0022 : ('MSG_TYPE_EXT_SEND_REQ', 2, 'f', 'f'),
                0x0023 : ('MSG_TYPE_EXT_SEND_ACK', 2, 'f', 'f'), 
                
                0x0030 : ('MSG_TYPE_SET_BALANCING',1,'B'),
                0x0031 : ('MSG_TYPE_SET_STRATEGY',1,'B'),
                
                0x0032 : ('MSG_TYPE_SET_MONITOR',1,'B'),
                0x0033 : ('MSG_TYPE_SET_MONITOR_BOUND', 2, 'f', 'f'),
                
                0x0034 : ('MSG_TYPE_SET_BC_SETTINGS',5,'B','B','B','B','B'),
                0x0035 : ('MSG_TYPE_SET_BC_PERIOD',1,'f'),
                0x0036 : ('MSG_TYPE_SET_BC_BOUND',1,'f'),
                
                0x0037 : ('MSG_TYPE_SET_OCV_BALANCING',1,'B'),
                0x0038 : ('MSG_TYPE_SET_OCV_LATENCY',1,'f'),
                
                0x0039 : ('MSG_TYPE_SET_U_TIMER',1,'B'),
                0x0040 : ('MSG_TYPE_SET_U_TIMER_VALUES',2,'L', 'L'),
                0x0041 : ('MSG_TYPE_SET_I_TIMER',1,'B'),
                0x0042 : ('MSG_TYPE_SET_I_TIMER_VALUES',2,'L', 'L'),
                0x0043 : ('MSG_TYPE_SET_KF_TIMER',1,'B'),
                0x0044 : ('MSG_TYPE_SET_KF_TIMER_VALUES',2,'L', 'L'),
                
                0x0050 : ('MSG_TIME_SET_PWM_PARAMS',5,'h','h','B','B','B'),                
                
                0x00A0 : ('MSG_TYPE_AUTODETECT_RANDOM ',8,'B','B','B','B','B','B','B','B'),
                0x00A1 : ('MSG_TYPE_AUTODETECT_PULSE ',0),
                0x00A2 : ('MSG_TYPE_AUTODETECT_ASSIGN ',1,'B'),
                
                0x0D00 : ('MSG_TYPE_DEBUG_SOC',1,'f'),
                0x0D01 : ('MSG_TYPE_DEBUG_VOLTAGE',1,'f'),
                                                
                
                0x0D10 : ('MSG_TYPE_DEBUG_SET_SCREEN',1,'B'),                
                0x0D11 : ('MSG_TYPE_DEBUG_SET_STATUS',1,'B'),
                0x0D12 : ('MSG_TYPE_DEBUG_SET_SOC',1,'f'),
                0x0D13 : ('MSG_TYPE_DEBUG_SET_VOLTAGE',1,'f'),
                0x0D14 : ('MSG_TYPE_DEBUG_SET_CURRENT',1,'f'),
                0x0D15 : ('MSG_TYPE_DEBUG_SET_ID',2,'B','B'),
                0x0D16 : ('MSG_TYPE_DEBUG_FORCE_SEND',0),
                    
                0x1FFF : ('MSG_TYPE_UNDEFINED',0)}
    
    # Byte-lengths of values:
    #  _________________________________
    # | 'f' | float32 | 4 byte | 32 bit |
    # | 'd' | float64 | 8 byte | 64 bit |
    # | 'B' |  char   | 1 byte |  8 bit |
    # | 'L' |  int32  | 4 byte | 32 bit |
    # |_____|_________|________|________|
    SIZES = {'f' : 4, 'd' : 8,  'B' : 1, 'L': 4, 'h': 2}
    
    # Possible states of a smartCell    
    STATUS = {0x00 : ('OFF','All switches off'),
              0x01 : ('SEND_UP','receiverID < senderID'),
              0x02 : ('SEND_DOWN','receiverID > senderID'),
              0x03 : ('RECEIVE_UP','receiverID > senderID'),
              0x04 : ('RECEIVE_DOWN','receiverID < senderID'),
              0x05 : ('FWD_START','Open forwarding loop'),
              0x06 : ('FWD_MID','Forward charge'),
              0x07 : ('FWD_END','Close forwarding loop'),
              0x08 : ('UNDEFINED','undefined setting'),
              0x09 : ('INITSEND','Send pulse for ID autodetect'),
              0x0A : ('INITRECEIVE','Receive pulse for ID autodetect'),
              0x0C : ('INITDISCHARGE',''),
              0x0B : ('BLOCKED_R','Switches off, blocked from right'),
              0xB0 : ('BLOCKED_L','Switches off, blocked from left'),
              0xBB : ('BLOCKED','Switches off, blocked from both sides')}