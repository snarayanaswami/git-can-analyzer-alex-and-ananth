from DataViewerView import DataViewerView
from pyqtgraph.Qt import QtGui
import sys

from mdbClient import mdbClient
from CANMessage import CANMessage, decodedCANMessage
from PyQt4.QtCore import QDateTime, QDate, QTime
import time


class DataViewerController:
    def __init__(self):
        self.app = QtGui.QApplication(sys.argv)
        self.DataViewer = DataViewerView()
        self.dMsg = decodedCANMessage()
        self.viewList = {}
        self.selectedEntries = []
        self.connectDB()
        
        self.DataViewer.ui.btnViewSelected.clicked.connect(self.btnViewSelectedClicked)
#         self.DataViewer.ui.DBTree.itemDoubleClicked.connect(self.ItemDblClicked)
        self.DataViewer.ui.btnSave.clicked.connect(self.saveCSV)
        self.DataViewer.ui.tblMessageView.cellClicked.connect(self.DataViewer.tblMessageViewDblClicked)
        self.DataViewer.ui.acnDropCollection.triggered.connect(self.deleteEntry)
        self.DataViewer.ui.acnRefreshCollections.triggered.connect(self.refreshCollections)
        self.DataViewer.ui.acnConnect.triggered.connect(self.connectDB)
        self.DataViewer.ui.acnViewTransactions.triggered.connect(self.viewTransactions)
    
    def connectDB(self):
            self.mdbc = mdbClient()
            if self.mdbc.connected:
                self.DataViewer.ui.acnConnect.setChecked(True)
                self.DataViewer.updateDBTree(self.mdbc)
            
    def refreshCollections(self):
        if self.mdbc.connected:
            self.DataViewer.updateDBTree(self.mdbc)
    
    def ItemDblClicked(self, item, column):
        if item.parent().parent().text(0)!=None:
            db = self.mdbc.client[str(item.parent().parent().text(0))]
            posts = db[str(item.parent().text(1))]
            
        messageList = []
        for post in posts.find({"CANid": int(item.type())}).sort("timestamp"):
            messageList.append(self.PostToCANMessage(post))
        self.viewList.append(messageList)
        
        starttime = time.gmtime(list(posts.find({}))[0]["abstimestamp"]/1000000)
        endtime = time.gmtime(list(posts.find({}))[-1]["abstimestamp"]/1000000)
        
        self.DataViewer.ui.edtStartTime.setDateTime(QDateTime(QDate(starttime[0],starttime[1],starttime[2]),QTime(starttime[3],starttime[4],starttime[5])))
        self.DataViewer.ui.edtEndTime.setDateTime(QDateTime(QDate(endtime[0],endtime[1],endtime[2]),QTime(endtime[3],endtime[4],endtime[5])))
        
        self.DataViewer.setPlotData(self.viewList)

    def PostToCANMessage(self, post):
        CurrentMessage = CANMessage(None,None,None,[],None,None,None,None)
        for field in post:
            CurrentMessage[field] = post[field]
        return CurrentMessage
    
    def deleteEntry(self):
        if len(self.DataViewer.ui.DBTree.selectedItems())>0:
            if self.DataViewer.ui.DBTree.selectedItems()[0].parent()!=None:
                if self.DataViewer.ui.DBTree.selectedItems()[0].parent().parent() is None:
                    dbName = str(self.DataViewer.ui.DBTree.selectedItems()[0].parent().text(0))
                    collectionName = str(self.DataViewer.ui.DBTree.selectedItems()[0].text(1))
                    print(dbName)
                    print(collectionName)
                    reply = QtGui.QMessageBox.question(self.DataViewer, 'Delete Collection', "All recorded Data in collection '{}' from database '{}' will be lost. \nSure?".format(collectionName, dbName), QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
                    if self.mdbc.connected:
                        db = self.mdbc.client[dbName]
                        collection = db[collectionName]
                        
                    if reply == QtGui.QMessageBox.Yes:
                        collection.drop()
                        if len(db.collection_names())==0:
                            db.connection.drop_database(dbName)
                        self.refreshCollections()
            else:
                dbName = str(self.DataViewer.ui.DBTree.selectedItems()[0].text(0))
                reply = QtGui.QMessageBox.question(self.DataViewer, 'Delete Database', "All recorded Data in '{}' will be lost. \nSure?".format(dbName), QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
                if self.mdbc.connected and reply == QtGui.QMessageBox.Yes:
                    db = self.mdbc.client[dbName]
                    db.connection.drop_database(dbName)
                    self.refreshCollections()
        

    def btnViewSelectedClicked(self):
        self.buildSelectedEntries()
        self.DataViewer.setPlotData(self.viewList)

    def buildSelectedEntries(self):
        self.threads = []
        # iterate over toplevel items (days with records)
        for n in xrange(self.DataViewer.ui.DBTree.topLevelItemCount()):
            # iterate over first level children (recordings on days)
            for i in xrange(self.DataViewer.ui.DBTree.topLevelItem(n).childCount()):
                # iterate over second level children (CANids in recordings)
                for j in xrange(self.DataViewer.ui.DBTree.topLevelItem(n).child(i).childCount()):
                    selectedEntry = [self.DataViewer.ui.DBTree.topLevelItem(n).text(0),self.DataViewer.ui.DBTree.topLevelItem(n).child(i).text(1),self.DataViewer.ui.DBTree.topLevelItem(n).child(i).child(j).type()]
                    # if item in tree is checked add it to selectedEntries
                    if self.DataViewer.ui.DBTree.topLevelItem(n).child(i).child(j).checkState(0)==2:
                        if selectedEntry not in self.selectedEntries:
                            self.selectedEntries.append(selectedEntry)
                            self.buildViewList(selectedEntry)
                    else:
                        if selectedEntry in self.selectedEntries:
                            self.selectedEntries.remove(selectedEntry)
                            self.viewList.pop(selectedEntry[2])
        


    def buildViewList(self, selectedEntry):
        db = self.mdbc.client[str(selectedEntry[0])]
        posts = db[str(selectedEntry[1])]        
        messageList = []
        for post in posts.find({"CANid": int(selectedEntry[2])}).sort("timestamp"):
            messageList.append(self.PostToCANMessage(post))
        self.viewList[selectedEntry[2]] = messageList
        
    def viewTransactions(self):
        if len(self.DataViewer.ui.DBTree.selectedItems())>0:
            if self.DataViewer.ui.DBTree.selectedItems()[0].parent()!=None:
                if self.DataViewer.ui.DBTree.selectedItems()[0].parent().parent() is None:
                                        
                    record = self.DataViewer.ui.DBTree.selectedItems()[0]
                    
                    for index in range(record.childCount()):
                        currentID = record.child(index)
                        if str(currentID.text(1)) == 'MSG_TYPE_VOLTAGE':
                            currentID.setCheckState(0,2)
                        if str(currentID.text(1)) == 'MSG_TYPE_DEBUG_VOLTAGE':
                            currentID.setCheckState(0,2)
                        if str(currentID.text(1)) == 'MSG_TYPE_SEND_ACK':
                            currentID.setCheckState(0,2)
                        
                    self.buildSelectedEntries()
                    self.DataViewer.setPlotData(self.viewList)
                    

    def saveCSV(self):        
#         for record in self.viewList:
        pass
        
#         self.app.processEvents()


if ( __name__ == '__main__' ):
    app = DataViewerController()
    if ( app ):
        app.app.exec_()