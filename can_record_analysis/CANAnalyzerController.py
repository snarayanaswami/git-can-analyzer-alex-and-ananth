from CANAnalyzerView import CANAnalyzerView
from CANMessageRecorder import CANMessageRecorder

from CANSnifferThreaded import CANSniffer

import sys
from PyQt4 import QtGui
from pyqtgraph.Qt import QtCore
from _collections import deque
from CANMessage import decodedCANMessage
from DATEviewerController import DATEviewerController
import CANSnifferSerial
from _struct import pack
import pyqtgraph

class CANAnalyzerController:
    def __init__(self):
#         pyqtgraph.setConfigOption('background', (100,100,100,50))
#         pyqtgraph.setConfigOption('foreground', 'k')
        self.app = QtGui.QApplication(sys.argv)
        self.window = CANAnalyzerView()
        
        self.dMsg = decodedCANMessage()
        
        self.CANSniffer = CANSniffer()
#         self.CANSniffer = CANSnifferSerial.CANSniffer()
        self.CANMessageRing = []
                
        # Initialize and connect timer to update the cycletime plot
        self.updCycleTmr = QtCore.QTimer()
        self.updCycleTmr.timeout.connect(self.updateCyclePlot)
        self.updCycleTmr.start(500)
        
        # Initialize and connect timer to update data plot
        self.updDataTmr = QtCore.QTimer()
        self.updDataTmr.timeout.connect(self.updateDataPlot)
        
        self.window.ui.chbLive.stateChanged.connect(self.setupdDataTmr)
        self.window.ui.spnInterval.valueChanged.connect(self.updDataTmr.setInterval)
        self.window.ui.CANTable.itemDoubleClicked.connect(self.TableItemDblClicked)
        self.window.ui.actionConnect.setChecked(self.CANSniffer.initialized)
        self.window.ui.actionConnect.triggered.connect(self.ConnectRelease)
        self.Recorder = CANMessageRecorder(self.CANSniffer)
        self.window.ui.btnRec.clicked.connect(self.StartCANMessageRecorder)
        self.window.ui.btnStop.clicked.connect(self.StopCANMessageRecorder)
        self.window.ui.btnViewRecord.clicked.connect(self.ViewRecord)
        self.window.ui.btnResize.clicked.connect(self.window.ui.CANRecordTable.resizeColumnsToContents)
        self.window.ui.actionSorted_by_TimeStamp.triggered.connect(self.SaveCSVFileTS)
        self.window.ui.actionSorted_by_CANid.triggered.connect(self.SaveCSVFileID)
        self.window.ui.actionNew.triggered.connect(self.NewRecord)
        self.window.ui.actionTo_MongoDB.triggered.connect(self.SaveToMongoDB)
        self.window.ui.dbgSend.clicked.connect(self.dbgSendMessage)
        self.window.ui.ctrlEnableBalancing.clicked.connect(self.ctrlEnableBalancing)
        self.window.ui.ctrlDisableBalancing.clicked.connect(self.ctrlDisableBalancing)
        self.window.ui.ctrlStartTransaction.clicked.connect(self.ctrlStartTransaction)
        self.window.ui.ctrlSetStrategy.clicked.connect(self.ctrlSetStrategy)
        self.window.ui.ctrlSetID.clicked.connect(self.ctrlSetID)
        self.window.ui.ctrlSetOptions.clicked.connect(self.ctrlSetOptions)
        self.window.ui.ctrlSetOCV.clicked.connect(self.ctrlSetOCV)
        self.window.ui.ctrlSetUIKF.clicked.connect(self.ctrlSetUIKF)
        self.window.ui.ctrlSetPWM.clicked.connect(self.ctrlSetPWM)        
        self.window.ui.actionClear.triggered.connect(self.clearData)
        self.window.ui.actionViewDATEviewer.triggered.connect(self.viewDATEviewer)
        self.app.aboutToQuit.connect(self.ApplicationCleanUp)

    def viewDATEviewer(self):
        if hasattr(self, 'DATEviewer'):
            self.DATEviewer.ui.show()
        else:
            self.DATEviewer = DATEviewerController(3, parent = self.window.ui, CS = self.CANSniffer)

    def clearData(self):
        self.CANMessageRing = []
        self.CANSniffer.CANMessageList = []
        self.CANSniffer.m_LastMsgsList = []
        
    def updateCyclePlot(self):
        self.window.UpdateTableItems(self.CANSniffer.CANMessageList)
        
        # Clear plotting area before repainting on canvas
        self.window.ClearCyclePlot()
        
        for i, CurrentMsg in enumerate(self.CANSniffer.CANMessageList):            
            # Update CyclePlot with latest Message in CANMessageList
            self.window.UpdateCyclePlot(CurrentMsg, len(self.CANSniffer.CANMessageList))
            
            # If a new CANid has been added to CANMessageList add another slot in CANMessageRing
            if len(self.CANMessageRing)<len(self.CANSniffer.CANMessageList):                    
                self.CANMessageRing.append(deque(maxlen=500)) 
            
            # If CANMessageRing deque is empty, append the first Item
            if len(self.CANMessageRing[i]) < 1:
                self.CANMessageRing[i].append(CurrentMsg)
            
            # If CurrentMsg's timestamp is newer that the last in CANMessageRing, append CurrentMessage to Ring
            # TODO -> Move this step to a faster timer to always append newest messages to CANMessageRing
            # Not too important because CANMessageRing is only used for live-plotting
#             if CurrentMsg.timestamp.millis > self.CANMessageRing[i][-1].timestamp.millis:
            if CurrentMsg.abstimestamp > self.CANMessageRing[i][-1].abstimestamp:
                self.CANMessageRing[i].append(CurrentMsg)


    def updateDataPlot(self):
        # Clear plotting area before repainting on canvas
        self.window.ClearDataPlot()
        
        if self.window.ui.chbLive.isChecked():
            length = self.window.ui.spnDisplayLength.value()
            self.window.UpdateDataPlot(self.CANMessageRing, length, self.CANSniffer.initTime)
           

    def setupdDataTmr(self):
        if self.window.ui.chbLive.isChecked():
            self.updDataTmr.start(self.window.ui.spnInterval.value())
        else:
            self.updDataTmr.stop()
            

    def TableItemDblClicked(self, item):  #TODO encapsulate in CANAnalyzer View and model as array or dictionary of values
        lastMessage = sorted(self.CANSniffer.CANMessageList)[item.row()]
        dMsg = decodedCANMessage(lastMessage)
        self.window.ui.lblCANid.setText('0x{0:08X}'.format(lastMessage.CANid))
        self.window.ui.lblLastUpdated.setText('{0:.2f}'.format(lastMessage.timestamp.millis/1000))
        self.window.ui.lblMeanCycle.setText('{0:.2f}'.format(lastMessage.cycle))
        self.window.ui.lblMSGType.setText('{}'.format(decodedCANMessage.MSG_TYPE[dMsg.msgtype][0]))
        self.window.ui.lblOrigin.setText('Cell {}'.format(dMsg.origin))
        if dMsg.target == 0xFF:
            self.window.ui.lblTarget.setText('Broadcast (0xFF)')
        else:
            self.window.ui.lblTarget.setText('Cell {}'.format(dMsg.target))
        
        if dMsg.msgtype == 0x0001:
            self.window.ui.lblValue.setText('{0:.3f} %'.format(dMsg.values[0]))
        elif dMsg.msgtype == 0x0002:
            self.window.ui.lblValue.setText('{0:.3f} Volt'.format(dMsg.values[0]))
        else:
            self.window.ui.lblValue.setText('No Voltage / SoC')
        
        if dMsg.msgtype == 0x0020:
            self.window.ui.lblStatus.setText('{} [{}]'.format(decodedCANMessage.STATUS[dMsg.values[0]][0], dMsg.values[0]))
            self.window.ui.lblDescription.setText('{}'.format(decodedCANMessage.STATUS[dMsg.values[0]][1]))
        else:
            self.window.ui.lblStatus.setText('No Status')
            self.window.ui.lblDescription.setText('-')

    def ConnectRelease(self):
        if self.CANSniffer.initialized==True:
            self.CANSniffer.release()
        else:
            if self.CANSniffer.connect():
                self.CANSniffer.StartReadThread()

    def StartCANMessageRecorder(self):
        self.Recorder.StartRecMessages()
        self.window.ui.btnRec.setEnabled(False)
        self.window.ui.btnStop.setEnabled(True)


    def StopCANMessageRecorder(self):
        self.Recorder.StopRecMessage()
        self.window.ui.btnStop.setEnabled(False)
        self.window.ui.btnRec.setEnabled(True)
    
    def ViewRecord(self):
        self.window.UpdateCANRecordTable(self.Recorder.RecordedMessages, self.Recorder.initTime)
        self.window.ui.CANRecordTable.resizeColumnsToContents()

    def SaveCSVFileTS(self):
        self.Recorder.SaveCSVFile("TimeStamp")
    
    def SaveCSVFileID(self):
        self.Recorder.SaveCSVFile("CANid")
        
    def SaveToMongoDB(self):
        self.Recorder.SaveToMongoDB()

    def NewRecord(self):
        reply = QtGui.QMessageBox.question(self.window, 'New CAN-Record', "All recorded Data will be lost. \nSure?", QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.Yes:
            self.window.ResetCANRecordTable()

    def dbgSendMessage(self):
        CANid = int(str(self.window.ui.dbgCANid.text()),16)
        length = self.window.ui.dbgLength.value()
        data = [self.window.ui.dbgData_0.text(), self.window.ui.dbgData_1.text(), self.window.ui.dbgData_2.text(), self.window.ui.dbgData_3.text(),
                self.window.ui.dbgData_4.text(), self.window.ui.dbgData_5.text(), self.window.ui.dbgData_6.text(), self.window.ui.dbgData_7.text()]
        self.CANSniffer.SendMessage(CANid, data, length)

    def ctrlEnableBalancing(self):
        extID = self.dMsg.getExtID('MSG_TYPE_SET_BALANCING', 0xFF, 0xFF)
        data = [str(1)]
        self.CANSniffer.SendMessage(extID, data, 1)
    
    def ctrlDisableBalancing(self):
        extID = self.dMsg.getExtID('MSG_TYPE_SET_BALANCING', 0xFF, 0xFF)
        data = [str(0)]
        self.CANSniffer.SendMessage(extID, data, 1)
    
    def ctrlStartTransaction(self):
        origin = int(str(self.window.ui.ctrlReceiver.text()),16)
        target = int(str(self.window.ui.ctrlSender.text()),16)
        
        extID = self.dMsg.getExtID('MSG_TYPE_DEBUG_FORCE_SEND', origin, target)
        data = []
        self.CANSniffer.SendMessage(extID, data, 0)
    
    def ctrlSetStrategy(self):
        strategy = self.window.ui.ctrlStrategyList.currentRow()
        extID = self.dMsg.getExtID('MSG_TYPE_SET_STRATEGY', 0xFF, 0xFF)
        data = [str(strategy)]
        self.CANSniffer.SendMessage(extID, data, 1)
    
    def ctrlSetID(self):
        target = int(str(self.window.ui.ctrlNode.text()),16)
        extID = self.dMsg.getExtID('MSG_TYPE_DEBUG_SET_ID', 0xFF, target)
        data = [self.window.ui.ctrlID.text(), self.window.ui.ctrlNUMOFNODES.text()]
        self.CANSniffer.SendMessage(extID, data, 2)
        
    def ctrlSetOCV(self):
        extID = self.dMsg.getExtID('MSG_TYPE_SET_OCV_BALANCING', 0xFF, 0xFF)
        data = [str(int(self.window.ui.chbOCVbalancing.isChecked()))]
        self.CANSniffer.SendMessage(extID, data, 1)
        
        extID = self.dMsg.getExtID('MSG_TYPE_SET_OCV_LATENCY', 0xFF, 0xFF)
        OCVlatency = pack('f', self.window.ui.spbOCVlatency.value())
        data = []
        for val in OCVlatency:
            data.append(hex(ord(val)))
        self.CANSniffer.SendMessage(extID, data, 4)
        
    def ctrlSetUIKF(self):
        extID = self.dMsg.getExtID('MSG_TYPE_SET_U_TIMER', 0xFF, 0xFF)
        data = [str(int(self.window.ui.chbUTimerEnabled.isChecked()))]
        self.CANSniffer.SendMessage(extID, data, 1)
        
        extID = self.dMsg.getExtID('MSG_TYPE_SET_U_TIMER_VALUES', 0xFF, 0xFF)
        uFrequency = pack('L', self.window.ui.spbUFrequency.value())
        uFilter    = pack('L', self.window.ui.spbUFilter.value())
        data = []
        for val in uFrequency:
            data.append(hex(ord(val)))
        for val in uFilter:
            data.append(hex(ord(val)))
        self.CANSniffer.SendMessage(extID, data, 8)
        
        extID = self.dMsg.getExtID('MSG_TYPE_SET_I_TIMER', 0xFF, 0xFF)
        data = [str(int(self.window.ui.chbITimerEnabled.isChecked()))]
        self.CANSniffer.SendMessage(extID, data, 1)
        
        extID = self.dMsg.getExtID('MSG_TYPE_SET_I_TIMER_VALUES', 0xFF, 0xFF)
        iFrequency = pack('L', self.window.ui.spbIFrequency.value())
        iFilter    = pack('L', self.window.ui.spbIFilter.value())
        data = []
        for val in iFrequency:
            data.append(hex(ord(val)))
        for val in iFilter:
            data.append(hex(ord(val)))
        self.CANSniffer.SendMessage(extID, data, 8)
        
        
        extID = self.dMsg.getExtID('MSG_TYPE_SET_KF_TIMER', 0xFF, 0xFF)
        data = [str(int(self.window.ui.chbKFTimerEnabled.isChecked()))]
        self.CANSniffer.SendMessage(extID, data, 1)
        
        extID = self.dMsg.getExtID('MSG_TYPE_SET_KF_TIMER_VALUES', 0xFF, 0xFF)
        kfFrequency = pack('L', self.window.ui.spbKFFrequency.value())
        kfFilter    = pack('L', self.window.ui.spbKFFilter.value())
        data = []
        for val in kfFrequency:
            data.append(hex(ord(val)))
        for val in kfFilter:
            data.append(hex(ord(val)))
        self.CANSniffer.SendMessage(extID, data, 8)
        
    
    def ctrlSetPWM(self):
        extID = self.dMsg.getExtID('MSG_TIME_SET_PWM_PARAMS', 0xFF, 0xFF)

        pwmFrequency   = pack('h', self.window.ui.spbPwmFrequency.value())
        numberOfPulses = pack('h', self.window.ui.spbNumberOfPulses.value())
        chargePercent    = pack('B', self.window.ui.spbChargePercent.value())
        dischargePercent = pack('B', self.window.ui.spbDischargePercent.value())
        deadtimePercent  = pack('B', self.window.ui.spbDeadtimePercent.value())
        
        data = []
        for val in pwmFrequency:
            data.append(hex(ord(val)))
        for val in numberOfPulses:
            data.append(hex(ord(val)))
        for val in chargePercent:
            data.append(hex(ord(val)))
        for val in dischargePercent:
            data.append(hex(ord(val)))
        for val in deadtimePercent:
            data.append(hex(ord(val)))
        
        self.CANSniffer.SendMessage(extID, data, 7)
        
        
    def ctrlSetOptions(self):
        # send message for enabling/disabling monitoring
        self.CANSniffer.SendMessage(0x0032FFFF, [str(int(self.window.ui.chbMonitoringEnabled.isChecked()))], 1)
        # send message containing upper and lower bound
        if self.window.ui.chbMonitoringEnabled.isChecked():
            startDiff = pack('f', self.window.ui.spbStartDiff.value())
            stopDiff = pack('f', self.window.ui.spbStopDiff.value())
            data = []
            for val in startDiff:
                data.append(hex(ord(val)))
            for val in stopDiff:
                data.append(hex(ord(val)))
            self.CANSniffer.SendMessage(0x0033FFFF, data, 8)
        
        # send message containing broadcasting options
        data = []
        data.append(str(int(self.window.ui.chbMinBC.isChecked())))
        data.append(str(int(self.window.ui.chbMaxBC.isChecked())))
        data.append(str(int(self.window.ui.chbTimebasedBC.isChecked())))
        data.append(str(int(self.window.ui.chbBCAfterTransaction.isChecked())))
        data.append(str(int(self.window.ui.chbLimitTimebasedBC.isChecked())))
        data.append(str(int(self.window.ui.chbBroadcastDebugStatus.isChecked())))
        self.CANSniffer.SendMessage(0x0034FFFF, data, 6)
        
        # send message containing broadcasting period
        bcPeriod = pack('f', self.window.ui.spbBCPeriod.value())
        data = []
        for val in bcPeriod:
            data.append(hex(ord(val)))
        self.CANSniffer.SendMessage(0x0035FFFF, data, 4)
    
        # send message containing broadcasting bound
        bcBound = pack('f', self.window.ui.spbLimitBound.value())
        data = []
        for val in bcBound:
            data.append(hex(ord(val)))
        self.CANSniffer.SendMessage(0x0036FFFF, data, 4)
        
    
    def ApplicationCleanUp(self):
        self.CANSniffer.release()


if ( __name__ == '__main__' ):
    app = CANAnalyzerController()
    if ( app ):
        app.app.exec_()