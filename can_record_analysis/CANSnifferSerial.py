import serial
from CANMessage import CANMessage, decodedCANMessage
from _struct import unpack
import time
import threading

###*****************************************************************
### CANSniffer structure used to receive CAN messages
### from Arduino CAN Shield adapter
### Make sure to use the Arduino Code in folder
### 
###*****************************************************************
class CANSniffer:
    def __init__(self, COMport = 'COM24', BAUDrate = 115200):
        self.COMport = COMport
        self.BAUDrate = BAUDrate
        self.CANMessageList = []
        self.initialized = False
        self.initTime = time.mktime(time.localtime()) - time.mktime(time.gmtime()) +  time.mktime(time.localtime())
        
    def connect(self):
        try:
            self.serial = serial.Serial(self.COMport, self.BAUDrate)
            self.initialized = True
        except:
            self.initialized = False
            print("Init Error!")
            
        if (self.serial.read(1) == chr(16)) and (self.serial.read(1) == chr(32)):
            print("Initialization done.")
            
        return self.initialized
    
    def release(self):
        try:
            self.serial.close()
            self.initialized = False
            print("Release done.")
        except:
            print("Release Error!")
        
    def StartReadThread(self):
        self.ReadThread = threading.Thread(name='ReadingThread', target=self.ReadMsgThread)
        self.tGoOn=True
        self.ReadThread.start()
            
    def StopReadThread(self):
        self.tGoOn=False
        
    def ReadMsgThread(self):
        print("ReadMsgThread started")
        while (self.tGoOn):
            while self.serial.readable():
                if (self.serial.read(1) == chr(0)) and (self.serial.read(1) == chr(255)):
                    CurrentLine = self.serial.read(ord(self.serial.read(1)))
                else:
                    CurrentLine = 0
                CurrentMsg = CANMessage(None, None, None, [], None, None, None, None)
                CurrentMsg.cycle = 0
                CurrentMsg.CANid = unpack('l', CurrentLine[0:4])[0]
                CurrentMsg.length = ord(CurrentLine[4])
                for i in range(CurrentMsg.length):
                    CurrentMsg.data.append(hex(ord(CurrentLine[5+i])))
                CurrentLine[5+CurrentMsg.length:5+CurrentMsg.length+4]
                CurrentMsg.abstimestamp = unpack('l',CurrentLine[5+CurrentMsg.length:5+CurrentMsg.length+4])[0]

                self.ProcessMessage(CurrentMsg)
        print("ReadMsgThread terminated")
        
    def ProcessMessage(self, NewMsg):
        found = False
        index = 0
        for index, CurrentMsg in enumerate(self.CANMessageList):
            if NewMsg.CANid == CurrentMsg.CANid:
                NewMsg.cycle = NewMsg.abstimestamp - CurrentMsg.abstimestamp
                del self.CANMessageList[index]
                self.CANMessageList.insert(index-1, NewMsg)
                found = True
        
        if not found:
            self.CANMessageList.append(NewMsg)
            
    def SendMessage(self):
        pass

        
if ( __name__ == '__main__' ):
    CANSniffer = CANSniffer()
    
    if CANSniffer.initialized==True:
        CANSniffer.release()
    else:
        if CANSniffer.connect():
            CANSniffer.StartReadThread()
    while 1:
        for msg in CANSniffer.CANMessageList:
            dMsg = decodedCANMessage(msg)
            print(dMsg)
        time.sleep(1)
        pass