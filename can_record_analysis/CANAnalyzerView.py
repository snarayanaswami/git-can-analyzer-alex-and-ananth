from PyQt4 import QtGui, uic
from pyqtgraph.functions import mkPen
from _collections import deque
from struct import unpack
from struct import pack
from CANMessage import decodedCANMessage
from PyQt4.Qt import QListWidgetItem

class CANAnalyzerView(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QDialog.__init__(self)
    
        # Set up the user interface from Designer.
        self.ui = uic.loadUi("./gui/CANAnalyzer.ui")
        self.ui.show()
        
        # Initialize plot labels
        self.ui.CANCyclePlot.setLabel('left', 'Cycletime', units='s')
        self.ui.CANCyclePlot.setLabel('bottom', 'Message ID', units='')
        self.ui.CANDataPlot.setLabel('left', 'Value', units='V')
        self.ui.CANDataPlot.setLabel('bottom', 'Time', units='s')
        self.legend = self.ui.CANDataPlot.addLegend(offset=-50)
        self.plotColorMap = {}
        self.dbgTypeListInitItems()
        self.ConnectSignals()
        
    def ResetCANRecordTable(self):
        self.ui.CANRecordTable.setRowCount(0)
        
    def UpdateCANRecordTable(self, RecordedMessages, initTime):
        if self.ui.CANRecordTable.rowCount() < len(RecordedMessages):
            rowCount = self.ui.CANRecordTable.rowCount()
            self.ui.CANRecordTable.setRowCount(len(RecordedMessages))                
            for i in xrange(rowCount, len(RecordedMessages)):
                dMsg = decodedCANMessage(RecordedMessages[i])
                self.ui.CANRecordTable.setItem(i, 0, QtGui.QTableWidgetItem('{0:.3f}'.format(RecordedMessages[i].abstimestamp / 1000000 - initTime)))
                self.ui.CANRecordTable.setItem(i, 1, QtGui.QTableWidgetItem(dMsg.MSG_TYPE[dMsg.msgtype][0]))
                self.ui.CANRecordTable.setItem(i, 2, QtGui.QTableWidgetItem(str(dMsg.origin)))
                self.ui.CANRecordTable.setItem(i, 3, QtGui.QTableWidgetItem(str(dMsg.target)))
                self.ui.CANRecordTable.setItem(i, 4, QtGui.QTableWidgetItem(str(RecordedMessages[i].cycle)))
                for n, dMsgValue in enumerate(dMsg.values):
                    if (n<4):
                        self.ui.CANRecordTable.setItem(i, n+5, QtGui.QTableWidgetItem(str(dMsgValue)))

    def ClearCyclePlot(self):
        self.ui.CANCyclePlot.plot(clear=True)
    
    def UpdateCyclePlot(self, CANMessage, length):
        dMsg = decodedCANMessage(CANMessage)
        
        xAxisCyclePlot = [dMsg.msgtype, dMsg.msgtype]
        yAxisCyclePlot = [0, float(CANMessage.cycle) / 1000]
        penCyclePlot = mkPen((dMsg.msgtype, length), width=10)
        self.ui.CANCyclePlot.plot(xAxisCyclePlot, yAxisCyclePlot, pen=penCyclePlot)
        
#         if (dMsg.msgtype != 0x0001) and (dMsg.msgtype != 0x0002):
#             if (dMsg.msgtype, dMsg.origin) not in self.plotColorMap.keys():
#                 self.plotColorMap[(dMsg.msgtype, dMsg.origin)] = penCyclePlot.color()
        
    def UpdateDataPlot(self, CANMessageRing, length, initTime):
        penc = 0
        for SingleCANMessageRing in CANMessageRing:
            # Only add Voltage and SOCplots to Dataplot window
            dMsg = decodedCANMessage(SingleCANMessageRing[0])
            if ( (not self.ui.chbShowDebug.isChecked() and ((dMsg.MSG_TYPE[dMsg.msgtype][0] == 'MSG_TYPE_SOC') or (dMsg.MSG_TYPE[dMsg.msgtype][0] == 'MSG_TYPE_VOLTAGE'))) or
                 (self.ui.chbShowDebug.isChecked() and  ((dMsg.MSG_TYPE[dMsg.msgtype][0] == 'MSG_TYPE_DEBUG_SOC') or (dMsg.MSG_TYPE[dMsg.msgtype][0] == 'MSG_TYPE_DEBUG_VOLTAGE'))) ):
                
                xAxisDataPlot = deque(maxlen=length)
                yAxisDataPlot = [deque(maxlen=length) for i in range(len(decodedCANMessage(SingleCANMessageRing[0]).values))]
                
                for CurrentMessage in SingleCANMessageRing:
                    dMsg = decodedCANMessage(CurrentMessage)
                    
                    
                    xAxisDataPlot.append(CurrentMessage.abstimestamp / 1000000 - initTime)
                    
                    for i, value in enumerate(dMsg.values):
                        yAxisDataPlot[i].append(value)
                
                for dataplot in yAxisDataPlot:
                    dMsg = decodedCANMessage(SingleCANMessageRing[0])
                    penDataPlot = mkPen((dMsg.origin, 5), width=1)
                    myplot = self.ui.CANDataPlot.plot(xAxisDataPlot, dataplot, pen=penDataPlot)
                    if (dMsg.msgtype, dMsg.origin) not in self.plotColorMap.keys():
                        self.legend.addItem(item=myplot, name = "<b>Cell #{}</b>".format(dMsg.origin))
                        self.plotColorMap[(dMsg.msgtype, dMsg.origin)] = penDataPlot.color()
                penc += 1
                
    
    def ClearDataPlot(self):
        self.ui.CANDataPlot.plot(clear=True)
            
    def UpdateTableItems(self, CANMessageList):
        self.ui.CANTable.resizeColumnsToContents()
        
        if self.ui.CANTable.rowCount() != len(CANMessageList):
            self.ui.CANTable.setRowCount(len(CANMessageList))

        for i, CurrentMsg in enumerate(sorted(CANMessageList)):
            dMsg = decodedCANMessage(CurrentMsg)
            self.ui.CANTable.setItem(i, 0, QtGui.QTableWidgetItem(dMsg.MSG_TYPE[dMsg.msgtype][0]))

            self.ui.CANTable.setItem(i, 2, QtGui.QTableWidgetItem(str(dMsg.target)))
            
            if (dMsg.msgtype, dMsg.origin) in self.plotColorMap.keys():
                tempItem = QtGui.QTableWidgetItem(str(dMsg.origin))
                tempItem.setBackground(QtGui.QBrush(self.plotColorMap[(dMsg.msgtype, dMsg.origin)]))
                self.ui.CANTable.setItem(i, 1, tempItem)
            else:
                self.ui.CANTable.setItem(i, 1, QtGui.QTableWidgetItem(str(dMsg.origin)))
            
    def dbgTypeListInitItems(self):
        for key in decodedCANMessage.MSG_TYPE:
            self.ui.dbgTypeList.addItem(QListWidgetItem(decodedCANMessage.MSG_TYPE[key][0], parent = None, type = key))

    def dbgTypeListItemClicked(self, item):
        self.updateCANidLabel()
     
    def updateCANidLabel(self):
        CANidString = "{TYPE:04X}{TARGET:02X}{ORIGIN:02X}".format(TYPE=self.ui.dbgTypeList.currentItem().type(), TARGET=self.ui.dbgTarget.value(), ORIGIN=self.ui.dbgOrigin.value())
        self.ui.dbgCANid.setText(CANidString)
        
        length = 0
        for i in range(2,decodedCANMessage.MSG_TYPE[self.ui.dbgTypeList.currentItem().type()][1]+2):
            length += decodedCANMessage.SIZES[decodedCANMessage.MSG_TYPE[self.ui.dbgTypeList.currentItem().type()][i]]  
        self.ui.dbgLength.setValue(length)
        
        
    def dbgValueChanged(self, value):
        CANtype = self.ui.dbgTypeList.currentItem().type()
        byteList = [chr(0) for i in range(8)]
        
        
        values = [self.ui.dbgValue.value(), self.ui.dbgValue2.value()]
        
        position = 0
        for i in range(2, decodedCANMessage.MSG_TYPE[CANtype][1]+2):
            length = decodedCANMessage.SIZES[decodedCANMessage.MSG_TYPE[CANtype][i]]
            packedValue = pack(decodedCANMessage.MSG_TYPE[CANtype][i], values[i-2])
            for n in range(length):
                byteList[n+position] = packedValue[n]
            position += length
        
        self.ui.dbgData_0.setText('{:02X}'.format(ord(byteList[0])))
        self.ui.dbgData_1.setText('{:02X}'.format(ord(byteList[1])))
        self.ui.dbgData_2.setText('{:02X}'.format(ord(byteList[2])))
        self.ui.dbgData_3.setText('{:02X}'.format(ord(byteList[3])))
        self.ui.dbgData_4.setText('{:02X}'.format(ord(byteList[4])))
        self.ui.dbgData_5.setText('{:02X}'.format(ord(byteList[5])))
        self.ui.dbgData_6.setText('{:02X}'.format(ord(byteList[6])))
        self.ui.dbgData_7.setText('{:02X}'.format(ord(byteList[7])))

    
    def ConnectSignals(self):
        self.ui.dbgTypeList.itemClicked.connect(self.dbgTypeListItemClicked)
        self.ui.dbgTarget.valueChanged.connect(self.updateCANidLabel)
        self.ui.dbgOrigin.valueChanged.connect(self.updateCANidLabel)
        self.ui.dbgValue.valueChanged.connect(self.dbgValueChanged)
        self.ui.dbgValue2.valueChanged.connect(self.dbgValueChanged)