from pymongo import MongoClient

class mdbClient:
    def __init__(self,host='localhost',port=27017):
        self.connected = self.createClient()
    
    def postRecord(self, RecordedCANMessages, DataBase, Collection):
        db = self.client[DataBase]
        collection = db[Collection]
        for CurrentMsg in RecordedCANMessages:
            collection.insert(CurrentMsg.DictionarizeMsg())
            
    def createClient(self,host='localhost',port=27017):
        try:
            self.client = MongoClient(host,port)
            return True
        except:
            print('Client not established...')
            return False