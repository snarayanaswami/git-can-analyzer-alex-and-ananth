System Requirements
-------------------
- A free USB port (USB 1.1 or USB 2.0) at the PC or at an USB hub connected
  to the PC
- Operating systems: Windows (XP, Vista, Windows 7) and Linux


Installation under Windows
--------------------------
We recommend that you setup the driver before connecting the PCAN-USB adapter
to the computer for the first time.


> Do the following to setup the driver:

1.  Please make sure that you are logged in as user with administrator
    privileges (not required for normal use of the PCAN-USB adapter later on).

2.  Start the driver setup program PeakOemDrv.exe.

3.  Follow the instructions of the setup program and install the driver for
    the PCAN-USB adapter.

Tip:
    During this procedure you can additionally select to install the CAN
    monitor PCAN-View for Windows.

> Do the following to connect the PCAN-USB adapter and complete the
  initialization:

1.  Connect the PCAN-USB adapter to an USB port of your PC. The PC can remain
    powered on.

2.  Windows reports that new hardware has been detected and possibly starts an
    installation wizard. This depends on the used Windows version. If
    applicable, confirm the steps for driver initialization.

After the installation process is completed successfully, the red LED on
the PCAN-USB adapter is illuminated.


Installation of PCAN hardware
-----------------------------
For information about the installation of PCAN hardware, please refer to the
user manual of the respective hardware. The hardware user manuals are located
in the folder <Product CD>\Pdf.

Please keep in mind that none of our setups install the PCANBasic.dll.
If you build an application that uses the PCANBasic.dll, you need to to copy
the PCANBasic.dll manually to your Windows System directory.

Windows 32-bit systems:
32-bit DLL > Windows\System32

Windows 64-bit systems:
32-bit DLL > Windows\SysWOW64
64-bit DLL > Windows\System32