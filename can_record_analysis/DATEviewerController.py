from CANSnifferThreaded import CANSniffer
from PyQt4 import QtGui, QtCore, uic
import sys
from CANMessage import decodedCANMessage
import pyqtgraph as pg
from pyqtgraph.functions import intColor, mkPen


class virtualSmartCell:
    def __init__(self, identifier):
        self.identifier = identifier
        self.voltageData = [3.4 for n in range(120)]
        self.timeData = [(n * 0.5) for n in range(120)]
        self.status = 0x00
        self.setTime = 0
    
    def updateStatus(self, status):
        self.status = status
        
    def updateVoltage(self, voltage):
        self.voltageData.append(voltage)
        theLen = len(self.voltageData)
        if(theLen)>120:
            self.voltageData = self.voltageData[theLen-120:]
            

class DATEviewerController(QtGui.QMainWindow):
    def __init__(self, cellCount, parent = None, CS = None):
        super(QtGui.QMainWindow, self).__init__(parent)
#         QtGui.QDialog.__init__(self)
        # Set up the user interface from Designer.
        self.ui = uic.loadUi("./gui/DATEviewer.ui")
        self.ui.show()
        self.initializeCellPlots(cellCount)
                
        if CS is None:
            self.CANSniffer = CANSniffer()
        else:
            self.CANSniffer = CS
            
        self.arrows = []
        
        self.smartCells = []
        self.modPlots = []
        
        for n in range(cellCount):
            self.smartCells.append(virtualSmartCell(identifier = n))
            self.modPlots.append(self.cellPlots[n].plot(y=[], pen=mkPen((n, 3))))
            
        self.connectGUIelements()
            
        # Initialize and connect timer to update the cycletime plot
        self.updateTimer = QtCore.QTimer()
        self.updateTimer.timeout.connect(self.updateData)
        self.updateTimer.start(500)
        pass
    
    def initializeCellPlots(self, cellCount):
        self.cellPlots = []
        for i in range(cellCount):
            self.cellPlots.append(self.ui.plotView.addPlot(title='Cell {}'.format(i+1)))
#             self.cellPlots[i].setYRange(2.7,3.4)
            self.cellPlots[i].setXRange(0,60)
            # TODO set correct labels for axis
#             self.cellPlots[i].setXLabel('time [s]')
#             self.cellPlots[i].setYLabel('voltage [V]')
        pass
    
    def connectGUIelements(self):
        self.ui.actionConnect.triggered.connect(self.ConnectRelease)
        

    sendBrush    = QtGui.QBrush(intColor(255))
    receiveBrush = QtGui.QBrush(intColor(10))
    
    upPosX      =  10
    downPosX    =  50
    sendPosY    =  3.1
    receivePosY =  3.2
    arrowSendUp = pg.ArrowItem(angle=-90, tipAngle=60, headLen=40, tailLen=40, tailWidth=20, brush=sendBrush)
    arrowSendDown = pg.ArrowItem(angle=-90, tipAngle=60, headLen=40, tailLen=40, tailWidth=20, brush=sendBrush)
    arrowReceiveUp = pg.ArrowItem(angle=90, tipAngle=60, headLen=40, tailLen=40, tailWidth=20, brush=receiveBrush)
    arrowReceiveDown = pg.ArrowItem(angle=90, tipAngle=60, headLen=40, tailLen=40, tailWidth=20, brush=receiveBrush)

    STATUS = {0x00 : ('OFF',None),
              0x01 : ('SEND_UP',arrowSendUp, upPosX, sendPosY),
              0x02 : ('SEND_DOWN',arrowSendDown, downPosX, sendPosY),
              0x03 : ('RECEIVE_UP',arrowReceiveUp, downPosX, receivePosY),
              0x04 : ('RECEIVE_DOWN',arrowReceiveDown, upPosX, receivePosY),
              0x05 : ('FWD_START','Open forwarding loop')}

    def setArrows(self):
        for arrow in self.arrows:
            for cellPlot in self.cellPlots:
                cellPlot.removeItem(arrow)
        self.arrows=[]
        
        for i, cell in enumerate(self.smartCells):
            if 0x00 < cell.status < 0x05:
                arrow = self.STATUS[cell.status][1]
                posX = self.STATUS[cell.status][2]
                posY = self.STATUS[cell.status][3]
                v = cell.voltageData
#                 theLen = len(v)
#                 midPt = theLen / 2.0 #v[math.floor(theLen / 2)]
#                 arrow.setPos(midPt+posX,posY)
                arrow.setPos(posX,posY)
                self.cellPlots[i].addItem(arrow)
                self.arrows.append(arrow)
        
    def ConnectRelease(self):
        if self.CANSniffer.initialized==True:
            self.CANSniffer.release()
        else:
            if self.CANSniffer.connect():
                self.CANSniffer.StartReadThread()
        self.ui.actionConnect.setChecked(self.CANSniffer.initialized)


    def updateData(self):
        if len(self.CANSniffer.CANMessageList)>0:
            for i, currentMessage in enumerate(self.CANSniffer.CANMessageList):
                dMsg = decodedCANMessage(currentMessage)
                
                # Voltage message received
                if (dMsg.MSG_TYPE[dMsg.msgtype][0] == 'MSG_TYPE_VOLTAGE'):
                    # Update data in virtual smartCell
                    self.smartCells[dMsg.origin].updateVoltage(dMsg.values[0])
                    
#                 if (dMsg.msgtype == 0x0020):
#                     self.smartCells[dMsg.origin].updateStatus(dMsg.values[0])
                
                # SendAcknowledge Message received
                if (dMsg.MSG_TYPE[dMsg.msgtype][0] == 'MSG_TYPE_SEND_ACK'):
                    sender = dMsg.origin
                    receiver = dMsg.target
                    
                    # Determine Receiver and Sender's status
                    if sender>receiver:
                        senderStatus = 0x01
                        receiverStatus = 0x03
                    else:
                        senderStatus = 0x02
                        receiverStatus = 0x04
                    
                    # Set virtual smartCell status accordingly
                    if currentMessage.timestamp.millis > self.smartCells[dMsg.origin].setTime:
                        self.smartCells[dMsg.origin].updateStatus(senderStatus)
                        self.smartCells[dMsg.origin].setTime = currentMessage.timestamp.millis
                        
                    if currentMessage.timestamp.millis > self.smartCells[dMsg.target].setTime:
                        self.smartCells[dMsg.target].updateStatus(receiverStatus)
                        self.smartCells[dMsg.target].setTime = currentMessage.timestamp.millis

                    
                # Reset cell statuses when unblock message is received
                if  (dMsg.MSG_TYPE[dMsg.msgtype][0] == 'MSG_TYPE_"UNBLOCK'):
                    for cell in self.smartCells:
                        if currentMessage.timestamp.millis > cell.setTime:
                            cell.updateStatus(0x00)
                            cell.setTime = currentMessage.timestamp.millis

            # draw voltageData into plots
            for i,cell in enumerate(self.smartCells):
                self.modPlots[i].setData(x=cell.timeData, y=cell.voltageData)
                
            #self.setArrows()
        pass
    
    def ApplicationCleanUp(self):
        self.CANSniffer.release()
    
if ( __name__ == '__main__' ):
    app = QtGui.QApplication([])
    DvC = DATEviewerController(3)
    app.aboutToQuit.connect(DvC.ApplicationCleanUp)
    if ( app ):
        app.exec_()