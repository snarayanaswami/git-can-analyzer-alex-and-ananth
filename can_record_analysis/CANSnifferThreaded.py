from PCANBasic import *
from CANMessage import CANMessage
from time import sleep
import threading
from uptime import boottime
import time
import datetime

###*****************************************************************
### Message Status structure used to show CAN Messages
### in a ListView
###*****************************************************************
class MessageStatus:
    def __init__(self, canMsg = TPCANMsg(), canTimestamp = TPCANTimestamp(), listIndex = -1):
        self.__m_Msg = canMsg
        self.__m_TimeStamp = canTimestamp
        self.__m_iIndex = listIndex
        self.__m_iCount = 1

    def getCANMessage(self):
        return self.__m_Msg

    def getTimestamp(self):
        return self.__m_TimeStamp

    def getPosition(self):
        return self.__m_iIndex

    def getCount(self):
        return self.__m_iCount

    def Actualize(self,canMsg,canTimestamp):
        self.__m_Msg = canMsg
        self.__m_TimeStamp = canTimestamp        
        self.__m_iCount = self.__m_iCount + 1
###*****************************************************************

###*****************************************************************
### CANSniffer structure used to receive CAN messages
### from PCAN USB adapter
###*****************************************************************
class CANSniffer:
    def __init__(self):
        self.m_LastMsgsList = []
        self.CANMessageList = []
        self.initialized = False
        self.initTime = time.mktime(time.localtime()) - time.mktime(time.gmtime()) +  time.mktime(time.localtime())
        self.m_objPCANBasic = PCANBasic()
        self.m_PcanHandle = PCAN_USBBUS1        
        self.baudrate = PCAN_BAUD_1M
        self.hwtype = PCAN_TYPE_ISA
        self.ioport = 0x2A0
        self.interrupt = 11

    def FormatCANMessage(self, msg, time):
        ListedMessage = CANMessage(None, None, None, [], None, None, None, None)
        newMsg = msg.getCANMessage()
        bIsRTR = (newMsg.MSGTYPE & PCAN_MESSAGE_RTR.value) == PCAN_MESSAGE_RTR.value
        
        if (newMsg.MSGTYPE & PCAN_MESSAGE_EXTENDED.value) == PCAN_MESSAGE_EXTENDED.value:
            if bIsRTR:
                strTemp = "EXT/RTR "
            else:
                strTemp = "EXTENDED"        
            ListedMessage.CANtype=strTemp
            ListedMessage.CANid=newMsg.ID          
        else:
            if bIsRTR:
                strTemp = "STD/RTR "
            else:
                strTemp = "STANDARD"
            ListedMessage.CANtype=strTemp
            ListedMessage.CANid=newMsg.ID        
        if bIsRTR:
            strTemp = "Remote Request"
        else:
            strTemp = ""
            for i in range(newMsg.LEN):
                ListedMessage.data.append(hex(newMsg.DATA[i]))
        ListedMessage.length=newMsg.LEN
        ListedMessage.count=msg.getCount()
        ListedMessage.cycle=float(time)
        ts = msg.getTimestamp()
        ListedMessage.timestamp=ts
        bootTime = (boottime()-datetime.datetime(1970,1,1))
        bootTimeInMicroSeconds = (bootTime.microseconds+(bootTime.seconds + bootTime.days * 24 * 3600) * 10**6)      
        ListedMessage.abstimestamp = float(ts.micros + 1000 * ts.millis + 0xFFFFFFFF * 1000 * ts.millis_overflow/1000)+bootTimeInMicroSeconds
        return ListedMessage 

    def GetFormatedError(self, error):
        stsReturn = self.m_objPCANBasic.GetErrorText(error, 0)
        if stsReturn[0] != PCAN_ERROR_OK:
            return "An error occurred. Error-code's text ({0:X}h) couldn't be retrieved".format(error)
        else:
            return stsReturn[1]
    
    def connect(self):
        result =  self.m_objPCANBasic.Initialize(self.m_PcanHandle,self.baudrate,self.hwtype,self.ioport,self.interrupt)
        if result != PCAN_ERROR_OK:
            print("Init Error!")
            self.initialized = False
        else:
            print("Initialization done.")
            self.initialized = True
        return self.initialized
     
    def release(self):
        self.StopReadThread()
        result = self.m_objPCANBasic.Uninitialize(self.m_PcanHandle)
        if result != PCAN_ERROR_OK:
            print("Release Error!")
        else:
            print("Release done.")
            self.initialized = False        
            
    def StartReadThread(self):
        self.ReadThread = threading.Thread(name='ReadingThread', target=self.ReadMsgThread)
        self.tGoOn=True
        self.ReadThread.start()
            
    def StopReadThread(self):
        self.tGoOn=False
        
    def ReadMsgThread(self):
        print("ReadMsgThread started")
        while (self.tGoOn):
            result = self.m_objPCANBasic.Read(self.m_PcanHandle)
            if (result[0] == PCAN_ERROR_OK):
                self.ProcessMessage(result[1:])
            else:
                sleep(0.05)
        print("ReadMsgThread terminated")
 

    def ProcessMessage(self, *args):
        bFound = False
        
        # Split the arguments. [0] TPCANMsg, [1] TPCANTimestamp
        #
        theMsg = args[0][0]
        itsTimeStamp = args[0][1]    

        # If ID and type of message is identical message will be updated
        for msgStsCurrentMessage in self.m_LastMsgsList:
            if msgStsCurrentMessage.getCANMessage().ID == theMsg.ID:
                if msgStsCurrentMessage.getCANMessage().MSGTYPE == theMsg.MSGTYPE:
                    bFound = True
                    break
                 
        if bFound:
            # ID is known in list, message will be updated
            self.UpdateMsg(msgStsCurrentMessage, theMsg, itsTimeStamp)
        else:
            # ID is new to list and a new entry is created
            self.AppendMsg(theMsg, itsTimeStamp)

    def AppendMsg(self, theMsg, itsTimeStamp):
        lastMsg = MessageStatus(theMsg,itsTimeStamp,len(self.m_LastMsgsList)) 
        self.m_LastMsgsList.append(lastMsg)
        self.CANMessageList.append(self.FormatCANMessage(lastMsg, "0"))
        
    def UpdateMsg(self, msgStsCurrentMessage, theMsg, itsTimeStamp):
        lastTimeStamp = msgStsCurrentMessage.getTimestamp()
        fTime = itsTimeStamp.millis + (itsTimeStamp.micros / 1000.0)       
        fTime = fTime - (lastTimeStamp.millis + (lastTimeStamp.micros / 1000.0))
        msgStsCurrentMessage.Actualize(theMsg, itsTimeStamp)
        del self.CANMessageList[msgStsCurrentMessage.getPosition()]
        self.CANMessageList.insert(msgStsCurrentMessage.getPosition(), self.FormatCANMessage(msgStsCurrentMessage, fTime))
        
    def SendMessage(self, CANid=1, data=[], length=0):       
        CANMsg = TPCANMsg()
        CANMsg.ID = CANid
        CANMsg.LEN = length
        CANMsg.MSGTYPE = PCAN_MESSAGE_EXTENDED
        for i in range(length):
            CANMsg.DATA[i] = int(str(data[i]),16)

        result = self.m_objPCANBasic.Write(self.m_PcanHandle, CANMsg)

        if result == PCAN_ERROR_OK:
            print("Message was successfully SENT")
        else:
            # An error occurred.  We show the error.
            #
            print(self.GetFormatedError(result))