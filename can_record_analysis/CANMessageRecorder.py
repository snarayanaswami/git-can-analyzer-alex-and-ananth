import threading
import time
from PyQt4 import QtGui
import csv
from mdbClient import mdbClient

class CANMessageRecorder():
    def __init__(self, CANSniffer):
        self.initialized = True
        self.CANSniffer = CANSniffer 
        
    def RecordCANMessages(self):
        print('Recording Thread started')
        self.RecordedMessages = []
        self.initTime = time.mktime(time.localtime()) - time.mktime(time.gmtime()) +  time.mktime(time.localtime())
        
        lastTimeStamp = 0
        
        tempFile = './tempRecord.csv'
        tempCSV = open(tempFile, 'wb')
        tempWriteOut = csv.writer(tempCSV, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        tempWriteOut.writerow(self.getCSVHeaderRow())
        
        while self.rGoOn:
            if len(self.RecordedMessages)<1:
                self.RecordedMessages.append(max(self.CANSniffer.CANMessageList, key=lambda CANMessage: CANMessage.abstimestamp))
                lastTimeStamp = self.RecordedMessages[-1].abstimestamp
            for CurrentMessage in self.CANSniffer.CANMessageList:
#                 if CurrentMessage.abstimestamp > self.RecordedMessages[-1].abstimestamp:
                if CurrentMessage.abstimestamp > lastTimeStamp:
                    self.RecordedMessages.append(CurrentMessage)
                    lastTimeStamp = CurrentMessage.abstimestamp
                    tempWriteOut.writerow(CurrentMessage.getCSVRow())
        print('Recording Thread terminated')
        tempCSV.close()
                       
    def StartRecMessages(self):
        self.RecMsgThread = threading.Thread(name='Record Message Thread', target=self.RecordCANMessages)
        self.rGoOn = True
        self.RecMsgThread.start()
        
    def StopRecMessage(self):
        self.rGoOn = False
        
    def SaveCSVFile(self, SortType):
        if SortType=="CANid":
            WriteOutList = sorted(self.RecordedMessages, key=lambda CANMessage: CANMessage.CANid)
        else:
            WriteOutList = self.RecordedMessages
        
        FileName = QtGui.QFileDialog.getSaveFileName(None, 'Save as...', "C:\ ", filter='*.csv')
        if FileName:
            print(FileName)
            with open(FileName, 'wb') as csvfile:
                CSVWriteOut = csv.writer(csvfile, delimiter=',',
                                        quotechar='|', quoting=csv.QUOTE_MINIMAL)
                CSVWriteOut.writerow(self.getCSVHeaderRow())  
                for CurrentMessage in WriteOutList:
                    CSVWriteOut.writerow(CurrentMessage.getCSVRow())
                    
    def getCSVHeaderRow(self):
        headerRow = ['Type', 'ID', 'Length']
        for i in range(8):
            headerRow.append('Data{}'.format(i))
        headerRow.append('Count')
        headerRow.append('Cycle')
        headerRow.append('Timestamp')
        return headerRow

    def SaveToMongoDB(self):
        try:
            mdbc = mdbClient()
            TimeStampFirstMessage = time.gmtime(self.RecordedMessages[0].abstimestamp/1000000)
            TimeStampLastMessage = time.gmtime(self.RecordedMessages[-1].abstimestamp/1000000)
            DBName = time.strftime("%Y_%m_%d", TimeStampFirstMessage)
            CollectionName = time.strftime("%H:%M:%S", TimeStampFirstMessage) + '_-_' + time.strftime("%H:%M:%S", TimeStampLastMessage)
            mdbc.postRecord(self.RecordedMessages, DBName, CollectionName)
        except:
            QtGui.QMessageBox.warning(None, 'MongoDB Error', 'A problem occured while connecting to the database.\nPlease make sure the MongoDB-Server is running', QtGui.QMessageBox.Ok, QtGui.QMessageBox.Ok)
