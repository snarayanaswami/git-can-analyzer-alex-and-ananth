import pyqtgraph as pg
from PyQt4 import QtGui, uic
from _collections import deque
from _struct import unpack
from pyqtgraph.functions import mkPen
from PyQt4.QtGui import QTreeWidgetItem
from CANMessage import decodedCANMessage
from math import ceil

class DataViewerView(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QDialog.__init__(self)
    
        # Set up the user interface from Designer.
        self.ui = uic.loadUi("./gui/DataViewer.ui")
        self.ui.show()
        
        # Initialize labels and generic UI elements
        self.ui.PlotDataView.setLabel('bottom', 'Time', units='s')
        self.region = pg.LinearRegionItem()
        self.region.setZValue(10)
        self.region.setRegion([5,10])
        self.connectSignals()

    def updateDBTree(self, mdbc):
        self.ui.DBTree.clear()
        
        MongoDBClient = mdbc.client
                
        for CurrentDB in MongoDBClient.database_names():
            if CurrentDB!='local' and CurrentDB!='admin':
                itm = QTreeWidgetItem()
                itm.setText(0,CurrentDB)
                for CurrentCollection in MongoDBClient[CurrentDB].collection_names():
                    if CurrentCollection!='system.indexes':
                        child = QTreeWidgetItem()
                        child.setText(0,'Collection:')
                        child.setText(1,CurrentCollection)
                        itm.addChild(child)
                        for CurrentIdentifier in sorted(MongoDBClient[CurrentDB][CurrentCollection].distinct('CANid')):
                            origin  = (CurrentIdentifier    ) & 0xFF;
                            target  = (CurrentIdentifier>>8 ) & 0xFF;
                            msgtype = (CurrentIdentifier>>16) & 0xFFFF;
                            
                            sgnl = QTreeWidgetItem(CurrentIdentifier)
                            sgnl.setCheckState(0,0)
                            sgnl.setText(0,'{} -> {}'.format(origin,target))
                            try:
                                sgnl.setText(1,'{}'.format(decodedCANMessage.MSG_TYPE[msgtype][0]))
                                
                                sgnl.setText(2,'Origin:')
                                sgnl.setText(3,'{}'.format(origin))
                                
                                sgnl.setText(4,'Target:')
                                sgnl.setText(5,'{}'.format(target))
                                
                                child.addChild(sgnl)
                            except:
                                print('Failed to print message with ID: {}'.format(msgtype))
                                
                            #for CurrentSignal in sorted(MongoDBClient[CurrentDB][CurrentCollection]) TODO -> categorize messages by first byte of data
                            # add checkable child for every value in the message (i.e. voltage value, cellid, cycle)
                self.ui.DBTree.addTopLevelItem(itm)

    def updatePlot(self):
        self.region.setZValue(10)
        minX, maxX = self.region.getRegion()
        self.ui.PlotDataView.setXRange(minX,maxX, padding=0, update=True)
                
    def updateRegion(self, window, viewRange):
        rgn = viewRange
        self.region.setRegion(rgn)
        
    def connectSignals(self):
        self.region.sigRegionChanged.connect(self.updatePlot)
        self.ui.PlotDataView.sigXRangeChanged.connect(self.updateRegion)
          
    def setPlotData(self, viewList, length=100):
        self.ui.PlotDataView.plot(clear=True)
        self.ui.PlotOverView.plot(clear=True)
        self.ui.PlotOverView.addItem(self.region, ignoreBounds=True)
        self.connectSignals()
        penc = 0
        
        initTime = 9999999999
        for key in viewList.keys():
            if viewList[key][0].abstimestamp/1000000 < initTime:
                initTime = viewList[key][0].abstimestamp/1000000
        
        messageList = []
        
#         for SinglePlot in viewList:
        for singleID in viewList.keys():
            xAxisDataPlot  = []
            yAxisDataPlot  = []
            yAxisCyclePlot = []
            dMsg = decodedCANMessage(viewList[singleID][0])
            
            if (dMsg.MSG_TYPE[dMsg.msgtype][0] == 'MSG_TYPE_VOLTAGE') or (dMsg.MSG_TYPE[dMsg.msgtype][0] == 'MSG_TYPE_DEBUG_VOLTAGE'):
            
                for CurrentMessage in viewList[singleID]:
                    xAxisDataPlot.append(CurrentMessage.abstimestamp/1000000-initTime)
                    dMsg = decodedCANMessage(CurrentMessage)                
                    if self.ui.chbValue.isChecked():
                        yAxisDataPlot.append(dMsg.values[0])
                    if self.ui.chbCycle.isChecked():
                        yAxisCyclePlot.append(CurrentMessage.cycle/1000)
            
                penDataPlot = mkPen(penc)
                
#                 xAxisDataPlot  = self.downSamplePlotList(xAxisDataPlot, 10000)
#                 yAxisDataPlot  = self.downSamplePlotList(yAxisDataPlot, 10000)
#                 yAxisCyclePlot = self.downSamplePlotList(yAxisCyclePlot, 10000)
                
                filterValue = self.ui.spbFilter.value()
                
                yAxisDataPlot  = self.filterPlotList(yAxisDataPlot,  filterValue)
                yAxisCyclePlot = self.filterPlotList(yAxisCyclePlot, filterValue)
            
                if self.ui.chbValue.isChecked():
                    self.ui.PlotDataView.plot(xAxisDataPlot,yAxisDataPlot, pen=penDataPlot, symbol=None)
                    self.ui.PlotOverView.plot(xAxisDataPlot,yAxisDataPlot, pen=penDataPlot)
                if self.ui.chbCycle.isChecked():
                    self.ui.PlotDataView.plot(xAxisDataPlot,yAxisCyclePlot, pen=penDataPlot, symbol='x')
                    self.ui.PlotOverView.plot(xAxisDataPlot,yAxisCyclePlot, pen=penDataPlot)
                penc+=13
                
                
            elif dMsg.MSG_TYPE[dMsg.msgtype][0] == 'MSG_TYPE_SEND_ACK':
                for currentMessage in viewList[singleID]:
                    messageList.append(currentMessage)
                    
                                        
            elif dMsg.MSG_TYPE[dMsg.msgtype][0] == 'MSG_TYPE_SEND_REQ':
                for currentMessage in viewList[singleID]:
                    messageList.append(currentMessage)
       
        self.setMessageView(messageList, initTime)
                
    def setMessageView(self, messageList = [], initTime = 0):
        self.ui.tblMessageView.setRowCount(0)
        
        for currentMessage in sorted(messageList, key = lambda CANMessage: CANMessage.abstimestamp):
            dMsg = decodedCANMessage(currentMessage)
            currentRow = self.ui.tblMessageView.rowCount()
            self.ui.tblMessageView.setRowCount(currentRow+1)
            self.ui.tblMessageView.setItem(currentRow, 0, QtGui.QTableWidgetItem(dMsg.MSG_TYPE[dMsg.msgtype][0]))
            self.ui.tblMessageView.setItem(currentRow, 1, QtGui.QTableWidgetItem(str(dMsg.origin)))
            self.ui.tblMessageView.setItem(currentRow, 2, QtGui.QTableWidgetItem(str(dMsg.target)))
            self.ui.tblMessageView.setItem(currentRow, 3, QtGui.QTableWidgetItem(str(currentMessage.count)))
            self.ui.tblMessageView.setItem(currentRow, 4, QtGui.QTableWidgetItem('{0:.3f}'.format(currentMessage.abstimestamp/1000000 - initTime)))
            
            if dMsg.MSG_TYPE[dMsg.msgtype][0] == 'MSG_TYPE_SEND_ACK':
                self.ui.tblMessageView.setItem(currentRow, 5, QtGui.QTableWidgetItem('Start of transfer from cell [{}] to cell [{}]'.format(dMsg.origin, dMsg.target)))
            elif dMsg.MSG_TYPE[dMsg.msgtype][0] == 'MSG_TYPE_SEND_REQ':
                self.ui.tblMessageView.setItem(currentRow, 5, QtGui.QTableWidgetItem('Request from cell [{}] to cell [{}]'.format(dMsg.origin, dMsg.target)))
            
        self.ui.tblMessageView.resizeColumnsToContents()
    
    def tblMessageViewDblClicked(self, row, column):
        time = float(self.ui.tblMessageView.item(row, 4).text())
        self.showOccurence(time)
        
    def showOccurence(self, time):
        self.region.setRegion((time-10, time+20))
            
    def downSamplePlotList(self, axisDataPlot, maxSamples):
        axisDataPlot_ds = []
        divider = ceil(len(axisDataPlot) / maxSamples)
        for i,value in enumerate(axisDataPlot):
            if (i%divider==0):
                axisDataPlot_ds.append(value)
        return axisDataPlot_ds
    
    def filterPlotList(self, axisDataPlot, filterval):
        axisDataPlot_fi = []
        try:
            ySum = axisDataPlot[0]*filterval
        except:
            ySum = 3.3*filterval
        for i, value in enumerate(axisDataPlot):
            ySum -= ySum/filterval
            ySum += value 
            axisDataPlot_fi.append(ySum/filterval)
        return axisDataPlot_fi