#---------------------------------------------------------------------------------
# Filename:        myCAN.py
# Project name:    Pi-CAN-Analyzer
# Author:          Hannes Bohnengel
# Supervisors:     Alexander Lamprecht, Swaminathan Narayanaswamy
# Last modified:   04 Oct 2017
# Comments:        This file contains some functions used for CAN communications 
#                  via the PICAN2 add-on board
#
#---------------------------------------------------------------------------------

# Import packages
import can
import os

# Define a class representing the CAN interface
class myCAN ():

	# Constructor
	def __init__(self, channel = 'can0', bustype = 'socketcan', ID = 0x7de):
		self.channel = channel
		self.bustype = bustype
		self.ID = ID
		self.up = False
		self.bus = can.interface.Bus()
		self.notifier = None#can.Notifier()

	# This function reads a system file which contains the state of the can0 interface
	# When it is down this file contains "down\n" and when it is up it contains "unknown\n"
	def check(self):
		result = -1

		# In this file the state of the can0 device is stored under Linux
		filename = "/sys/class/net/can0/operstate"

		if os.path.isfile(filename):
			f = open(filename, "r")
			tmp = f.readline()
			if tmp == "down\n":
				result = 0
			elif tmp == "unknown\n":
				result = 1
		else:
			result = -1
		return result

	# This function checks if the CAN interface can0 is already set up and enabled
	# If so, it creates a bus instance
	def setup(self):
		result = -1
		tmp = self.check()
		if tmp == 1:
			# create a bus instance (configured in ~/.can)
			try:
				self.bus = can.interface.Bus(self.channel, self.bustype)
				result = 0
			except:
				print("\nError: Can not create a bus instance!")
		elif tmp == 0:
			print("\nCAN interface not enabled! Execute startCAN.sh\nas root and restart the program.")
		else:
			print("\nInterface 'can0' not found!")
		return result

	# This function sends some data over the CAN bus
	def send(self, msg):
		# Creating CAN message
		canMsg = can.Message(arbitration_id=self.ID, data=msg, extended_id=False)
		# Sending message
		try:
			self.bus.send(canMsg)
			return 0
		except:
			return -1

	# This function starts a CAN bus listener which prints received message
	# to the terminal
	def startListener(self):
		try:
			self.notifier = can.Notifier(self.bus, [can.Printer()])
			print("\nListening for messages. Press ENTER to stop.\n")
			print("Uptime / Address / Type / Length / Data\n")
		except:
			print("\nSomething went wrong while starting CAN bus listener!")

	# This function stops the respective listener
	def stopListener(self):
		try:
			self.notifier.stop()
			print("\nCAN Notifier stopped.")
		except:
			print("Something went wrong while stopping CAN bus listener!")



