# What is in here?

This folder contains some config files, which have been edited on the Raspberry Pi. The following table shows the files and their respective path.

File | Path
------ | ------
[`config.txt`](config.txt)     | `/etc/config.txt`
[`dnsmasq.conf`](dnsmasq.conf) | `/etc/dnsmasq.conf`
[`dnsmasq`](dnsmasq)           | `/etc/default/dnsmasq`
[`hostapd.conf`](hostapd.conf) | `/etc/hostapd/hostapd.conf`
[`hostapd`](hostapd)           | `/etc/default/hostapd`
[`interfaces`](interfaces)     | `/etc/network/interfaces`
