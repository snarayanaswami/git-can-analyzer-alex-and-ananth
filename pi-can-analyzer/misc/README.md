# What is in here?

This folder contains ...

Folder/File | Description
------ | ------
[`PICAN2_Schematics_RevB.pdf`](PICAN2_Schematics_RevB.pdf) | Schematics of the PICAN2 add-on board
[`PICAN2_UserGuide_RefB_V1.2.pdf`](PICAN2_UserGuide_RefB_V1.2.pdf) | User guide of the PICAN2 add-on board
[`CellDischargeTrim.csv`](CellDischargeTrim.csv) | This file contains the original discharge voltage measurement of a single cell. The mapping is timestamp to voltage, beginning from 100% and ending at 0%. 
[`VoltageToSoC.csv`](VoltageToSoC.csv) | This file contains a mapping from voltage to state of charge, based on [`CellDischargeTrim.csv`](CellDischargeTrim.csv).
[`VoltageToSoC.xlsx`](VoltageToSoC.xlsx) | This file contains the calculation between timestamp and respective state of charge as well as a plot which shows the voltage characteristics of a cell.
