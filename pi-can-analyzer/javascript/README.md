# What is in here?

This folder contains the JavaScript based scripts to run a socket server for the communication between a Python based application and any web client on a host.

Folder | Description
------ | ------
[`webHandler`](webHandler) | This folder contains the whole JavaScript source code, both server and client side scripts and the necessary libraries.
