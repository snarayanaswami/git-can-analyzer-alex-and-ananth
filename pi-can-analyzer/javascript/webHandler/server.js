
/*---------------------------------------------------------------------------------
	Filename:        server.js
	Project name:    Pi-CAN-Analyzer
	Author:          Hannes Bohnengel
	Supervisors:     Alexander Lamprecht, Swaminathan Narayanaswamy
	Last modified:   19 Sep 2017
	Comments:        This is a javascript file containing the server code.
                   See: https://www.youtube.com/watch?v=2hhEOGXcCvg
---------------------------------------------------------------------------------*/

// Set this flag to true if you want to suppress the printed output
var daemonized = true;

// For ports lower than 1024 the script has to be executed as root
// (Port 80 is the default port for HTTP)
var myPort = 80;

// Import express module
var express = require('express');

// Import socket module
var socket = require('socket.io');

// Create express application
var app = express();

// Start express server listening on a port for requests
server = app.listen(myPort);

// Set up directory to use for server application (static files)
app.use(express.static('public'));

// Print something to the console
if(!daemonized) console.log("Socket server is running on port " + myPort);

// Create an I/O handler
var io = socket(server);

// React to new connections
io.sockets.on('connection', newConnection);

function newConnection(socket) {

	// Print a message each time a new connection is built
	if(!daemonized) console.log('New connection: ' + socket.id);

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Event 2
	// Listen for messages with event name 'msg'
	socket.on('msg', testData);

	// Function handler for forwarding data
	function testData(data) {

		// Choose one of the following options
		// Option 1: Send to all other hosts except the sender
		socket.broadcast.emit('msg', data);
		// Option 2: Send to all hosts which are connected to this socket
		//io.sockets.emit('msg', data);

		// Print received data
		//console.log("Received: " + data + " from " + socket.id);
	}
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Event 2
	// Listen for messages with event name 'vol'
	socket.on('vol', forwardVoltage);

	// Function handler for forwarding voltage values
	function forwardVoltage(data) {

		// Choose one of the following options
		// Option 1: Send to all other hosts except the sender
		socket.broadcast.emit('vol', data);
		// Option 2: Send to all hosts which are connected to this socket
		//io.sockets.emit('msg', data);

		// Print received data
		//console.log("Received: " + data + " from " + socket.id);
	}
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}
