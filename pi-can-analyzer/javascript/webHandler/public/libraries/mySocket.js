
/*---------------------------------------------------------------------------------
	Filename:        mySocket.js
	Project name:    Pi-CAN-Analyzer
	Author:          Hannes Bohnengel
	Supervisors:     Alexander Lamprecht, Swaminathan Narayanaswamy
	Last modified:   12 Sep 2017
	Comments:        This is a javascript file containing all socket realted code.
---------------------------------------------------------------------------------*/

var y_pos1, y_pos2 = 500;

// Global flag for socket connection
var connected = false;

// Variable which stores the socket.io object
var mySocket;

// Connect to a specific socket server
function connectSocket(address, port) {
	// address could be "http://" +:
	// - raspberrypi.local
	// - localhost
	// - 10.0.0.5
	server = "http://" + address + ":" + port;

	//console.log("Connecting to socket server " + server);

	// Connect to socket server
	mySocket = io.connect(server);

	// Set handlers for default events
	mySocket.on('connect', onConnect);
	mySocket.on('reconnect', onReconnect);
	mySocket.on('disconnect', onDisconnect);
	mySocket.on('connect_error', onConnectionError);
}

// Handler for 'connect' event
function onConnect() {
	if(connected == false) {
		console.log("Connected to " + server);
		connected = true;
	}
}

// Handler for 'connect' event
function onReconnect() {
	console.log("Reconnected to " + server);
	connected = true;
}

// Handler for 'connect' event
function onDisconnect() {
	if (connected == true) {
		console.log("Disconnected from " + server);
		connected = false;
	}
}

// Handler for 'connect' event
function onConnectionError() {
	console.log("Connection Error");
}

// Set up a handler for a specific event
function listenSocket(event, handler) {
	mySocket.on(event, handler);
}

// Stop listening to a specific event
function stopListeningSocket(event, handler) {
	mySocket.off();
}

// Send something over the socket
function sendSocket(event, data) {
  mySocket.emit(event, data);
}



// ##############################
// CLEAN UP AFTER HERE !!!!
// ##############################


/*
function toHexString(byteArray) {
  return Array.from(byteArray, function(byte) {
    return ("0" + (byte & 0xFF).toString(16)).slice(-2);
  }).join(" ")
}
*/

function getData() {
  if (check1.checked) {
    mySocket.on("msg", printData);
		console.log("Socket ON");
  }
  else {
    mySocket.off("msg");
		console.log("Socket OFF !!!");
  }
	function printData(data) {

		var text;

		// Create data string to be printed
		//datastring = "Origin: 0x" + data[0] + "; Target: 0x" + data[1] + "; MsgType: 0x" + data[2];

		// remove the first three elements of the data list to be able to 
		// print the remaining data bytes in a row
		//data.shift();
		//data.shift();
		//data.shift();

		// add data bytes to data string
		//datastring += (";  Data: " + toHexString(data));

		// crate DOM element to be printed
		//text = createP(datastring);
		text = createP(data);
		text.position(check1.x, y_pos2);

		// increment y-position of text
    y_pos2 += ySpace;
  }
}
