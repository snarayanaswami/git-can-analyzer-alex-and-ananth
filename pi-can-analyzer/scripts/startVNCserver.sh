#!/bin/bash

#######################################################################################
#
# Filename:        startVNCserver.sh
# Author:          Hannes Bohnengel
# Last modified:   26 Sep 2017
#
# Comments:
# -------------
# This is a bash script to start a VNC server on the Raspberry Pi 3
# To be used like this (<PORT> is optionally):
# $ ./startVNCserver.sh <PORT>
#
# To connect to the VNC server install vncviewer from the following url:
# https://www.realvnc.com/en/connect/download/viewer/
# and execute it like this (for <PORT> set in the respective port):
# $ vncviewer raspberrypi.local:<PORT>
#######################################################################################

# Get port from first argument (1 is converted to port 5901)
if [ -z "$1" ]
  then
		PORT=1
else
	PORT=$1
fi

# Specifying the resolution
RES="1280x1024"

# Define command
CMD="vncserver :$PORT -geometry $RES -depth 16 -pixelformat rgb565"

# Output:
echo "Starting a VNC server with the following command:"
echo "$CMD"
${CMD}
