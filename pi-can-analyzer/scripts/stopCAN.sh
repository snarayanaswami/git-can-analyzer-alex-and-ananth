#!/bin/bash

######################################################################################
#
# Filename:        stopCAN.sh
# Author:          Hannes Bohnengel
# Last modified:   26 Sep 2017
#
# Comments:
# -------------
# This is a bash script to bring down the CAN interface available on the PICAN2 add-on
# board and has to be executed as root.
#
######################################################################################

# Output:
echo "Bringing down the CAN interface on can0"
ip link set can0 down

