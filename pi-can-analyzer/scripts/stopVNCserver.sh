#!/bin/bash

#######################################################################################
#
# Filename:        stopVNCserver.sh
# Author:          Hannes Bohnengel
# Last modified:   26 Sep 2017
#
# Comments:
# -------------
# This is a bash script to stop a VNC server on the Raspberry Pi 3
# To be used like this (<PORT> is optionally):
# $ ./stopVNCserver.sh <PORT>
#
#######################################################################################

# Get port from first argument (1 is converted to port 5901)
if [ -z "$1" ]
  then
		PORT=1
else
	PORT=$1
fi

# Define command
CMD="vncserver -kill :$PORT"

# Output:
echo "Stopping the VNC server with the following command:"
echo "$CMD"
$CMD
