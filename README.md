# git-can-analyzer-alex-and-ananth

This repository contains the source code for two CAN analyzing applications for the Smart Cell Demonstrator.

Folder | Description
------ | ------
[can_record_analysis](can_record_analysis) | This folder contains the Python and Qt based source code for the CAN analyzer software to be run on a Windows computer. 
[pi-can-analyzer](pi-can-analyzer) | This folder contains the Python and JavaScript based source code for the cell status visualization to be run on the Raspberry Pi.

